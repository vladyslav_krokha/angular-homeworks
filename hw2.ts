import { fromEvent, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { calculateMortgage } from './calculate';

const loanAmount = document.querySelector('#loanAmount');
const loanInterest = document.querySelector('#loanInterest');
const loanLength = document.querySelector('#loanLength');
const res = document.querySelector('#result');

const startInputStream = el =>
  fromEvent(el, 'input').pipe(map(event => parseFloat(event.target.value)));

const loanAmount$ = startInputStream(loanAmount);
const loanInterest$ = startInputStream(loanInterest);
const loanLength$ = startInputStream(loanLength);

combineLatest([loanInterest$, loanAmount$, loanLength$])
  .pipe(
    map(([loanInterest$, loanAmount$, loanLength$]) => {
      return calculateMortgage(loanInterest$, loanAmount$, loanLength$);
    })
  )
  .subscribe(result => (res.innerHTML = result));